import 'dart:convert';
import 'dart:developer';

import 'package:chatapp2/api/api.dart';
import 'package:chatapp2/helper/dialogs.dart';
import 'package:chatapp2/main.dart';
import 'package:chatapp2/models/chat_user.dart';
import 'package:chatapp2/screens/profile_screen.dart';
import 'package:chatapp2/widgets/chat_user_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //for storing all user
  List<ChatUser> _listChatUser = [];

  //for storing searched items
  final List<ChatUser> _searchList = [];
  //for storing search status
  bool _isSearching = false;

  @override
  void initState() {
    super.initState();
    APIs.getSelfInfo();

    //for updating user active according to lifecycle events
    //resume -- active or online
    //pause -- inactive or offline
    SystemChannels.lifecycle.setMessageHandler((message) {
      log("<Message> : $message");
      if (APIs.auth.currentUser != null) {
        if (message.toString().contains('pause'))
          APIs.updateActiveStatus(false);
        if (message.toString().contains('resume'))
          APIs.updateActiveStatus(true);
      }
      return Future.value(message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      //for hiding keyboard when a tap is detected on screen
      onTap: () => FocusScope.of(context).unfocus(),
      child: WillPopScope(
          //if search is on & back button is pressed when close search
          //or else simple close curren screen on back button click
          onWillPop: () {
            if (_isSearching) {
              setState(() {
                _isSearching = !_isSearching;
              });
              return Future.value(false);
            } else {
              return Future.value(true);
            }
          },
          child: Scaffold(
              //app bar
              appBar: AppBar(
                leading: const Icon(CupertinoIcons.home),
                title: _isSearching
                    ? TextField(
                        decoration: const InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Name, Email, ...'),
                        autofocus: true,
                        style:
                            const TextStyle(fontSize: 17, letterSpacing: 0.5),
                        //when search changes then updated search list
                        onChanged: (value) {
                          //searcj logic
                          _searchList.clear();
                          for (var i in _listChatUser) {
                            if (i.name
                                    .toLowerCase()
                                    .contains(value.toLowerCase()) ||
                                i.email
                                    .toLowerCase()
                                    .contains(value.toLowerCase())) {
                              _searchList.add(i);
                            }
                            setState(() {
                              _searchList;
                            });
                          }
                        },
                      )
                    : const Text(
                        'We Chat',
                      ),
                actions: [
                  //search user button
                  IconButton(
                      onPressed: () {
                        setState(() {
                          _isSearching = !_isSearching;
                        });
                      },
                      icon: Icon(_isSearching
                          ? CupertinoIcons.clear_circled_solid
                          : Icons.search)),

                  //more features button
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => ProfileScreen(
                                      user: APIs.currentUserInfor,
                                    )));
                      },
                      icon: const Icon(Icons.more_vert))
                ],
              ),

              //floating button to add new user
              floatingActionButton: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: FloatingActionButton(
                  onPressed: () async {
                    //add chat user dialog
                    _addChatUserDialog();
                  },
                  child: const Icon(Icons.add_comment_rounded),
                ),
              ),

              //body list user chat
              body: StreamBuilder(
                  stream: APIs.getMyUsersId(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      //if data is loading
                      case ConnectionState.waiting:

                      //if some or all data is loaded then show it
                      case ConnectionState.active:
                      case ConnectionState.done:
                        return StreamBuilder(
                          stream: APIs.getAllUsers(),
                          builder: (context, snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.none:
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              //if data is loading
                              case ConnectionState.waiting:

                              //if some or all data is loaded then show it
                              case ConnectionState.active:
                              case ConnectionState.done:
                                if (snapshot.hasData) {
                                  final data = snapshot.data?.docs;
                                  _listChatUser = data
                                          ?.map((e) =>
                                              ChatUser.fromJson(e.data()))
                                          .toList() ??
                                      [];
                                  log('Data: ${_listChatUser}');
                                }
                                if (_listChatUser.isNotEmpty) {
                                  return ListView.builder(
                                      itemCount: _isSearching
                                          ? _searchList.length
                                          : _listChatUser.length,
                                      padding:
                                          EdgeInsets.only(top: mq.height * .01),
                                      physics: BouncingScrollPhysics(),
                                      itemBuilder: (context, index) {
                                        return ChatUserCard(
                                          user: _isSearching
                                              ? _searchList[index]
                                              : _listChatUser[index],
                                        );
                                      });
                                } else {
                                  return const Center(
                                    child: Text(
                                      'No Data Founded!',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  );
                                }
                            }
                          },
                        );
                    }
                  }))),
    );
  }

  //for adding new chat user
  void _addChatUserDialog() {
    String email = '';

    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              contentPadding: const EdgeInsets.only(
                  left: 24, right: 24, top: 20, bottom: 10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              //title
              title: const Row(
                children: [
                  Icon(
                    Icons.person_add,
                    color: Colors.blue,
                    size: 28,
                  ),
                  Text(' Add User')
                ],
              ),
              //content
              content: TextFormField(
                maxLines: null,
                onChanged: (value) => email = value,
                decoration: InputDecoration(
                    hintText: 'Email Id',
                    prefixIcon: const Icon(
                      Icons.email,
                      color: Colors.blue,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15))),
              ),

              //actions
              actions: [
                //cancel actions button
                MaterialButton(
                  onPressed: () {
                    //for hinding alert dialog
                    Navigator.pop(context);
                  },
                  child: const Text(
                    'Cancel',
                    style: TextStyle(fontSize: 16, color: Colors.blue),
                  ),
                ),

                //Add actions button
                MaterialButton(
                  onPressed: () async {
                    if (email.isNotEmpty)
                      await APIs.addChatUser(email).then((value) {
                        if (!value) {
                          Dialogs.showSnackbar(context, 'User does not Exists');
                        }
                        //for hidding dialog
                        Navigator.pop(context);
                      });
                  },
                  child: const Text(
                    'Add',
                    style: TextStyle(fontSize: 16, color: Colors.blue),
                  ),
                )
              ],
            ));
  }
}

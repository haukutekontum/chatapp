import 'dart:developer';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chatapp2/api/api.dart';
import 'package:chatapp2/helper/dialogs.dart';
import 'package:chatapp2/helper/my_date_util.dart';
import 'package:chatapp2/main.dart';
import 'package:chatapp2/models/chat_user.dart';
import 'package:chatapp2/screens/auth/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';

class ViewProfileScreen extends StatefulWidget {
  final ChatUser user;

  const ViewProfileScreen({super.key, required this.user});

  @override
  State<ViewProfileScreen> createState() => _ViewProfileScreenState();
}

class _ViewProfileScreenState extends State<ViewProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  String? _image = APIs.currentUserInfor.image;

  List<ChatUser> listChatUser = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //app bar
        appBar: AppBar(
          title: Text(
            widget.user.name,
          ),
        ),

        //Joined on
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Joined On: ',
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            Text(
              MyDateUtil.getlastMessageTime(
                  context: context, time: widget.user.createAt, hasYear: true),
              style: const TextStyle(
                color: Colors.black87,
                fontSize: 16,
              ),
            ),
          ],
        ),

        //body
        body: Form(
            key: _formKey,
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: mq.width * .05),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //adding some space
                      SizedBox(
                        width: mq.width,
                        height: mq.height * .05,
                      ),

                      //Image Proflie
                      Stack(
                        children: [
                          //local image
                          _image != null
                              ? ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(mq.height * .1),
                                  child: CachedNetworkImage(
                                    imageUrl: _image.toString(),
                                    width: mq.height * .2,
                                    height: mq.height * .2,
                                    fit: BoxFit.fill,
                                  ),
                                )
                              : Container(),
                        ],
                      ),

                      //adding some space
                      SizedBox(
                        height: mq.height * .03,
                      ),

                      //user email
                      Text(
                        widget.user.email,
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 16),
                      ),

                      //adding some space
                      SizedBox(
                        height: mq.height * .03,
                      ),

                      //user about
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "About: ",
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            widget.user.about,
                            style: const TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ))));
  }
}

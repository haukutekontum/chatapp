import 'dart:developer';

import 'package:chatapp2/api/api.dart';
import 'package:chatapp2/main.dart';
import 'package:chatapp2/provider/login_provider.dart';
import 'package:chatapp2/screens/auth/login_screen.dart';
import 'package:chatapp2/screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(const Duration(seconds: 2), () {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
          systemNavigationBarColor: Colors.white,
          statusBarColor: Colors.white));
      // if (APIs.auth.currentUser != null) {
      //   log("\nUser: ${APIs.auth.currentUser}");
      //   //navigate to home screen
      //   Navigator.pushReplacement(
      //       context, MaterialPageRoute(builder: (_) => const HomeScreen()));
      // }
      //  else
      //  {
      //navigate to login screen
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
        return ChangeNotifierProvider(
          create: (_) {
            return LoginProvider();
          },
          child: const LoginScreen(),
        );
      }));
      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    //initalizing media query (for getting device screen size)
    mq = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          'Welcome to We Chat',
        ),
      ),
      body: Stack(
        children: [
          //app logo
          Positioned(
              top: mq.height * .15,
              width: mq.width * .5,
              right: mq.width * .25,
              child: Image.asset('assets/images/chat_icon.png')),

          //google login button
          Positioned(
              bottom: mq.height * .15,
              width: mq.width,
              child: const Text(
                "MADE IN VIET NAM ❤️ ",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16, color: Colors.black87, letterSpacing: .5),
              ))
        ],
      ),
    );
  }
}

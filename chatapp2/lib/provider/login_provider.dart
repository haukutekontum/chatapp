import 'package:flutter/cupertino.dart';

class LoginProvider extends ChangeNotifier {
  bool _isAnimate = false;

  bool get isAnimate => _isAnimate;

  void finishAnimate (){
    _isAnimate = true;
    notifyListeners();
  }
}
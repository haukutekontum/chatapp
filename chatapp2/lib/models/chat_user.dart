class ChatUser {
  ChatUser({
    required this.image,
    required this.name,
    required this.about,
    required this.lastActive,
    required this.isOnline,
    required this.id,
    required this.createAt,
    required this.pushToken,
    required this.email,
  });
  late String image;
  late String name;
  late String about;
  late String lastActive;
  late bool isOnline;
  late String id;
  late String createAt;
  late String pushToken;
  late String email;

  ChatUser.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    name = json['name'];
    about = json['about'];
    lastActive = json['last_active'];
    isOnline = json['is_online'];
    id = json['id'];
    createAt = json['create_at'];
    pushToken = json['push_token'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['image'] = image;
    _data['name'] = name;
    _data['about'] = about;
    _data['last_active'] = lastActive;
    _data['is_online'] = isOnline;
    _data['id'] = id;
    _data['create_at'] = createAt;
    _data['push_token'] = pushToken;
    _data['email'] = email;
    return _data;
  }
}

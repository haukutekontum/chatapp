import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:chatapp2/models/chat_user.dart';
import 'package:chatapp2/models/message.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class APIs {
  //for authentication
  static FirebaseAuth auth = FirebaseAuth.instance;

  //for accessing cloud firestore database
  static FirebaseFirestore firestore = FirebaseFirestore.instance;

  //for accessing firestore storage
  static FirebaseStorage storage = FirebaseStorage.instance;

  //to return current user
  static User get currentUser => auth.currentUser!;

  //for storing self information
  static late ChatUser currentUserInfor;

  //for accessing firebase messaging token
  static FirebaseMessaging fMessaging = FirebaseMessaging.instance;

  //for getting firebase messaging (Push Notification)
  static Future<void> getFirebaseMessagingToken() async {
    await fMessaging.requestPermission();

    await fMessaging.getToken().then((value) {
      if (value != null) {
        currentUserInfor.pushToken = value;
        debugPrint('Push Token: $value');
      }
    });

    // for handling forground messages
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      debugPrint('Got a message whilst in the foreground');
      debugPrint('Message data: ${message.data}');

      if (message.notification != null) {
        debugPrint(
            'Message also contained a notification: ${message.notification}');
      }
    });
  }

  //for sending push notification
  static Future<void> sendPushNotification(
      ChatUser chatUser, String msg) async {
    try {
      final body = {
        "to": chatUser.pushToken,
        "notification": {
          "title": chatUser.name,
          "body": msg,
          "android_channel_id": "chats"
        },
        "data": {"some_data": "User ID: ${currentUserInfor.id}"}
      };

      var res =
          await http.post(Uri.parse('https://fcm.googleapis.com/fcm/send'),
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json',
                HttpHeaders.authorizationHeader:
                    'key=AAAA8rms2sQ:APA91bHvq_of8MCsPKxSMNMk1UE1Xnq4woEbwj7NHNE9y4rAYKBB6Mv4iQ5mQbRKj2XjcH5Ff26yUDXBxVXH2CCRcSh4hLjfJkBI2tRltq54iiJ1xCUfsOXSHbnkNDcw_RhY9WWJESP9'
              },
              body: jsonEncode(body));
      debugPrint('Response status: ${res.statusCode}');
      debugPrint('Response body: ${res.body}');
    } catch (e) {
      debugPrint('\nsendPushNotification: $e');
    }
  }

  //for checking if user exists or not?
  static Future<bool> userExist() async {
    return (await firestore.collection('users').doc(currentUser.uid).get())
        .exists;
  }

  //for adding an chat user for our converstion
  static Future<bool> addChatUser(String email) async {
    final data = await firestore
        .collection('users')
        .where('email', isEqualTo: email)
        .get();
    if (data.docs.isNotEmpty && data.docs.first.id != currentUser.uid) {
      //user exist

      debugPrint('User exist: ${data.docs.first.data()}');

      await firestore
          .collection('users')
          .doc(currentUser.uid)
          .collection('my_users')
          .doc(data.docs.first.id)
          .set({});
      return true;
    } else {
      //user doesn't exist
      return false;
    }
  }

  //for getting current user info
  static Future<void> getSelfInfo() async {
    await firestore
        .collection('users')
        .doc(currentUser.uid)
        .get()
        .then((value) async => {
              if (value.exists)
                // if users id exist then set value for currentUserInfor
                {
                  currentUserInfor = ChatUser.fromJson(value.data()!),
                  await getFirebaseMessagingToken(),

                  //for setting user status to active
                  APIs.updateActiveStatus(true)
                }
              else
                // else call Create User and then run this again
                {await createUser().then((value) => getSelfInfo())}
            });
  }

  //for creating a new user
  static Future<void> createUser() async {
    //get time today create this account
    final time = DateTime.now().millisecondsSinceEpoch.toString();

    final user = ChatUser(
        id: currentUser.uid,
        name: currentUser.displayName.toString(),
        email: currentUser.email.toString(),
        about: "Hey, I'm using We Chat",
        image: currentUser.photoURL.toString(),
        createAt: time,
        isOnline: false,
        lastActive: time,
        pushToken: '');

    return await firestore
        .collection('users')
        .doc(currentUser.uid)
        .set(user.toJson());
  }

  //for getting id's of known users from firestore database
  static Stream<QuerySnapshot<Map<String, dynamic>>> getMyUsersId() {
    return firestore
        .collection('users')
        .doc(currentUser.uid)
        .collection('my_users')
        .snapshots();
  }

  //for getting all users from firestore database
  static Stream<QuerySnapshot<Map<String, dynamic>>> getAllUsers() {
    return firestore
        .collection('users')
        .where('id', isNotEqualTo: currentUser.uid)
        .snapshots();
  }

  //for updating user information
  static Future<void> updateUserInfo() async {
    await firestore.collection('users').doc(currentUser.uid).update(
        {'name': currentUserInfor.name, 'about': currentUserInfor.about});
  }

  //update profile picture of user
  static Future<void> updateProfilePicture(File file) async {
    //check jpg or png
    final ext = file.path.split('.').last;
    final ref = storage.ref().child('profile_pictures/${currentUser.uid}.$ext');
    ref.putFile(file, SettableMetadata(contentType: 'image/$ext')).then((p0) {
      log('Data Transferred: ${p0.bytesTransferred / 1000} kb');
    }).then((value) async {
      //updatw image to current user information
      currentUserInfor.image = await ref.getDownloadURL();
      log('image HERE: ' + currentUserInfor.image);
      await firestore
          .collection('users')
          .doc(currentUser.uid)
          .update({'image': currentUserInfor.image});
    });
  }

  //for getting specific user info
  static Stream<QuerySnapshot<Map<String, dynamic>>> getUserInfo(
      ChatUser chatuser) {
    return firestore
        .collection('users')
        .where('id', isEqualTo: chatuser.id)
        .snapshots();
  }

  //update online or last active status of user
  static Future<void> updateActiveStatus(bool isOnline) async {
    firestore.collection('users').doc(currentUser.uid).update({
      'is_online': isOnline,
      'last_active': DateTime.now().millisecondsSinceEpoch.toString(),
      'push_token': currentUserInfor.pushToken
    });
  }

  /***************** Chat Screen Related APIS ****************/

  //chats (collection) --> conversation_id(doc) --> messages (collection) --> message (doc)

  //useful for getting conversation id
  //set the link user id 1 and user id 2
  //Example ojYUmGnG2aZm6wYR1gfarnhNUD73_UaCRIWntRPN72NjWORYL3cEIAPG3
  static String getConversationID(String id) =>
      currentUser.uid.hashCode <= id.hashCode
          ? '${currentUser.uid}_$id'
          : '${id}_${currentUser.uid}';

  //for getting all users from firestore database
  static Stream<QuerySnapshot<Map<String, dynamic>>> getAllMessages(
      ChatUser user) {
    return firestore
        .collection('chats/${getConversationID(user.id)}/messages/')
        .orderBy('sent', descending: true)
        .snapshots();
  }

  // for sending message
  static Future<void> sendMessage(ChatUser user, String msg, Type type) async {
    //message sending time (also used as id)
    final time = DateTime.now().millisecondsSinceEpoch.toString();

    //message to send
    final Message message = Message(
        msg: msg,
        read: '',
        toId: user.id,
        type: type,
        sent: time,
        fromId: currentUser.uid);

    final ref =
        firestore.collection('chats/${getConversationID(user.id)}/messages/');
    await ref.doc(time).set(message.toJson()).then((value) =>
        sendPushNotification(user, type == Type.text ? msg : 'image'));
  }

  //update read status of message
  static Future<void> updateMessageReadStatus(Message message) async {
    firestore
        .collection('chats/${getConversationID(message.fromId)}/messages/')
        .doc(message.sent)
        .update({'read': DateTime.now().millisecondsSinceEpoch.toString()});
  }

  //get only last message of a specific chat
  static Stream<QuerySnapshot<Map<String, dynamic>>> getLastMessage(
      ChatUser user) {
    return firestore
        .collection('chats/${getConversationID(user.id)}/messages/')
        .orderBy('sent', descending: true)
        .limit(1)
        .snapshots();
  }

  //send chat image
  static Future<void> sendChatImage(ChatUser chatUser, File file) async {
    //getting image file extension
    final ext = file.path.split('.').last;

    //storage file ref with path
    final ref = storage.ref().child(
        'images/${getConversationID(chatUser.id)}/${DateTime.now().millisecondsSinceEpoch}.$ext');

    //uploading file
    ref.putFile(file, SettableMetadata(contentType: 'image/$ext')).then((p0) {
      log('Data Transferred: ${p0.bytesTransferred / 1000} kb');
    }).then((value) async {
      //updating image in firestore database
      final imageUrl = await ref.getDownloadURL();
      await sendMessage(chatUser, imageUrl, Type.image);
    });
  }

  //delete message
  static Future<void> deleteMessage(Message message) async {
    await firestore
        .collection('chats/${getConversationID(message.toId)}/messages/')
        .doc(message.sent)
        .delete();

    if (message.type == Type.image) {
      await storage.refFromURL(message.msg).delete();
    }
  }

  //update message
  static Future<void> updateMessage(Message message, String updatedMsg) async {
    await firestore
        .collection('chats/${getConversationID(message.toId)}/messages/')
        .doc(message.sent)
        .update({'msg': updatedMsg});
  }
}

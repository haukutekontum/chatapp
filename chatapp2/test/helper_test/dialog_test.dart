import 'package:chatapp2/helper/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('showSnackBar displays Snackbar with correct',
      (widgetTester) async {
    const String snackbarMessage = 'This is a test message';

    await widgetTester.pumpWidget(MaterialApp(
      home: Builder(builder: (BuildContext context) {
        Dialogs.showSnackbar(context, snackbarMessage);
        return Container();
      }),
    ));
    await widgetTester.pump();

    expect(find.text(snackbarMessage), findsOneWidget);
    expect(find.byType(SnackBar), findsOneWidget);
  });
}

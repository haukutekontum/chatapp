import 'dart:developer';
import 'dart:io';

import 'package:chat_app/models/chat_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';

class APIs {
  //for authentication
  static FirebaseAuth auth = FirebaseAuth.instance;

  //for accessing cloud firestore database
  static FirebaseFirestore firestore = FirebaseFirestore.instance;

  //for accessing firestore storage
  static FirebaseStorage storage = FirebaseStorage.instance;

  //to return current user
  static User get currentUser => auth.currentUser!;

  //for storing self information
  static late ChatUser currentUserInfor;

  //for checking if user exists or not?
  static Future<bool> userExist() async {
    return (await firestore.collection('users').doc(currentUser.uid).get())
        .exists;
  }

  //for getting current user info
  static Future<void> getSelfInfo() async {
    await firestore
        .collection('users')
        .doc(currentUser.uid)
        .get()
        .then((value) async => {
              if (value.exists)
                {currentUserInfor = ChatUser.fromJson(value.data()!)}
              else
                {await createUser().then((value) => getSelfInfo())}
            });
  }

  //for creating a new user
  static Future<void> createUser() async {
    final time = DateTime.now().millisecondsSinceEpoch.toString();

    final user = ChatUser(
        id: currentUser.uid,
        name: currentUser.displayName.toString(),
        email: currentUser.email.toString(),
        about: "Hey, I'm using We Chat",
        image: currentUser.photoURL.toString(),
        createAt: time,
        isOnline: false,
        lastActive: time,
        pushToken: '');

    return await firestore
        .collection('users')
        .doc(currentUser.uid)
        .set(user.toJson());
  }

  //for getting all users from firestore database
  static Stream<QuerySnapshot<Map<String, dynamic>>> getAllUsers() {
    return firestore
        .collection('users')
        .where('id', isNotEqualTo: currentUser.uid)
        .snapshots();
  }

  //for updating user information
  static Future<void> updateUserInfo() async {
    await firestore.collection('users').doc(currentUser.uid).update(
        {'name': currentUserInfor.name, 'about': currentUserInfor.about});
  }

  //update profile picture of user
  static Future<void> updateProfilePicture(File file) async {
    //check jpg or png
    final ext = file.path.split('.').last;
    final ref = storage.ref().child('profile_pictures/${currentUser.uid}.$ext');
    ref.putFile(file, SettableMetadata(contentType: 'image/$ext')).then((p0) {
      log('Data Transferred: ${p0.bytesTransferred / 1000} kb');
    });

    currentUserInfor.image = await ref.getDownloadURL();
    log('image HERE: ' + currentUserInfor.image);
    await firestore
        .collection('users')
        .doc(currentUser.uid)
        .update({'image': currentUserInfor.image});
  }
}

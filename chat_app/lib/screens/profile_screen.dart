import 'dart:developer';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chat_app/api/api.dart';
import 'package:chat_app/helper/dialogs.dart';
import 'package:chat_app/main.dart';
import 'package:chat_app/models/chat_user.dart';
import 'package:chat_app/screens/auth/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';

class ProfileScreen extends StatefulWidget {
  final ChatUser user;

  const ProfileScreen({super.key, required this.user});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  String? _image = APIs.currentUserInfor.image;

  List<ChatUser> listChatUser = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //app bar
        appBar: AppBar(
          title: const Text(
            'Profile Screen',
          ),
        ),

        //floating button to add new user
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: FloatingActionButton.extended(
            backgroundColor: Colors.redAccent,
            onPressed: () async {
              //for showing progress dialog
              Dialogs.showProgressBar(context);
              // sign out from app
              await APIs.auth.signOut().then((value) async {
                await GoogleSignIn().signOut().then((value) {
                  //for hiding progress dialog
                  Navigator.pop(context);
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (_) => LoginScreen()));
                });
              });
            },
            icon: const Icon(Icons.logout),
            label: const Text('Logout'),
          ),
        ),

        //body
        body: Form(
            key: _formKey,
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: mq.width * .05),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //adding some space
                      SizedBox(
                        width: mq.width,
                        height: mq.height * .05,
                      ),

                      //Image Proflie
                      Stack(
                        children: [
                          //local image
                          _image != null
                              ? ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(mq.height * .1),
                                  child: Image.file(
                                    File(_image!),
                                    width: mq.height * .2,
                                    height: mq.height * .2,
                                    fit: BoxFit.fill,
                                  ),
                                )
                              : Container(),

                          //edit button image
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: MaterialButton(
                              onPressed: () {
                                _showBottomSheet();
                              },
                              shape: const CircleBorder(),
                              elevation: 1,
                              child: Icon(
                                Icons.edit,
                                color: Colors.blue,
                              ),
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),

                      //adding some space
                      SizedBox(
                        height: mq.height * .03,
                      ),

                      //user email
                      Text(
                        widget.user.email,
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 16),
                      ),

                      //adding some space
                      SizedBox(
                        height: mq.height * .05,
                      ),

                      //user name
                      TextFormField(
                        initialValue: widget.user.name,
                        onSaved: (val) =>
                            APIs.currentUserInfor.name = val ?? '',
                        validator: (val) => val != null && val.isNotEmpty
                            ? null
                            : 'Required Field',
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12)),
                            prefixIcon: const Icon(
                              Icons.person,
                              color: Colors.blue,
                            ),
                            hintText: 'eg. Happy Fin',
                            label: Text('Name')),
                      ),

                      //adding some space
                      SizedBox(
                        height: mq.height * .05,
                      ),

                      //user about
                      TextFormField(
                        initialValue: widget.user.about,
                        onSaved: (val) =>
                            APIs.currentUserInfor.about = val ?? '',
                        validator: (val) => val != null && val.isNotEmpty
                            ? null
                            : 'Required Field',
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12)),
                            prefixIcon: const Icon(
                              Icons.info,
                              color: Colors.blue,
                            ),
                            hintText: 'eg. Feeling Happy',
                            label: Text('About')),
                      ),

                      //adding some space
                      SizedBox(
                        height: mq.height * .05,
                      ),

                      //update button
                      ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                            shape: StadiumBorder(),
                            minimumSize: Size(mq.width * .5, mq.height * .06)),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            log('inside validator');
                            _formKey.currentState!.save();
                            APIs.updateUserInfo().then((value) => {
                                  Dialogs.showSnackbar(
                                      context, 'Profile Updated Successfully!')
                                });
                          }
                        },
                        icon: const Icon(
                          Icons.edit,
                          size: 30,
                        ),
                        label: const Text(
                          'UPDATE',
                          style: TextStyle(fontSize: 16),
                        ),
                      )
                    ],
                  ),
                ))));
  }

  //bottom sheet for picking a profile picture for user
  void _showBottomSheet() {
    var logger = Logger();
    showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        builder: (_) {
          return Container(
            height: mq.height * .35,
            child: ListView(
              padding: EdgeInsets.only(
                  top: mq.height * .03, bottom: mq.height * .05),
              children: [
                //pick picture label
                const Text(
                  'Pick Proflie Picture',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),

                //add some space
                SizedBox(
                  height: mq.height * .02,
                ),

                //buttons
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //pick from gallery button
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            shape: const CircleBorder(),
                            fixedSize: Size(mq.width * .3, mq.height * .15)),
                        onPressed: () async {
                          final ImagePicker picker = ImagePicker();
                          // Pick an image.
                          final XFile? image = await picker.pickImage(
                              source: ImageSource.gallery);
                          if (image != null) {
                            debugPrint(
                                'Image Path: ${image.path} -- MimeType ${image.mimeType}');
                            setState(() {
                              _image = image.path;
                            });

                            APIs.updateProfilePicture(File(_image!));
                            //for hiding bottom sheet
                            Navigator.pop(context);
                          }
                        },
                        child: Image.asset('assets/images/galery.png')),

                    //take picture from camera button
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            shape: const CircleBorder(),
                            fixedSize: Size(mq.width * .3, mq.height * .15)),
                        onPressed: () async {
                          final ImagePicker picker = ImagePicker();
                          // Pick an image.
                          final XFile? image = await picker.pickImage(
                              source: ImageSource.camera);
                          if (image != null) {
                            log('Image Path: ${image.path} -- MimeType ${image.mimeType}');
                            setState(() {
                              _image = image.path;
                            });
                            APIs.updateProfilePicture(File(_image!));
                            //for hiding bottom sheet
                            Navigator.pop(context);
                          }
                        },
                        child: Image.asset('assets/images/camera.png'))
                  ],
                )
              ],
            ),
          );
        });
  }
}

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

//extension thêm phương thức mới cho kiểu dữ liệu
//ở đây thêm phương thức T (T là con của num)
//operator định nghĩa toán tử +-*/, ở đây là +
extension OptionalInfixAddition<T extends num> on T? {
  T? operator +(T? other) {
    //nếu this != 0 thì shadow nhận this, còn không thì shadow nhận other
    final shadow = this ?? other;
    if (shadow != null) {
      //trả về number shadow + other (other = 0 nếu orther = null)
      return shadow + (other ?? 0) as T;
    } else {
      return null;
    }
  }
}

extension OptionalInfixMinus<T extends num> on T? {
  T? operator -(T? other) {
    //nếu this != 0 thì shadow nhận this, còn không thì shadow nhận other
    final shadow = this ?? other;
    if (shadow != null) {
      //trả về number shadow + other (other = 0 nếu orther = null)
      return shadow - (other ?? 0) as T;
    } else {
      return null;
    }
  }
}

class Counter extends StateNotifier<int?> {
  Counter() : super(null);
  void increment() => state = state == null ? 0 : state + 1;

  void decreasement() => state = state == null ? 0 : state - 1;
}

final counterProvider =
    StateNotifierProvider<Counter, int?>((ref) => Counter());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData.dark(),
    );
  }
}

class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: Consumer(
          builder: (context, ref, child) {
            final count = ref.watch(counterProvider);
            final text = count == null ? 'Press the button' : count.toString();
            return Text(text);
          },
        ),
      ),
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        TextButton(
            onPressed: ref.read(counterProvider.notifier).increment,
            child: const Text('Increase counter')),
        TextButton(
            onPressed: ref.read(counterProvider.notifier).decreasement,
            child: const Text('Decrease counter')),
      ]),
    );
  }
}

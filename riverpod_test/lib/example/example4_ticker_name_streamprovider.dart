import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData.dark(),
    );
  }
}

const names = ['Alice', 'Bob', 'Charle', 'Dalvid', 'Eve', 'Ginny', 'Larry'];

//tạo một dòng stream phát ra các giá trị liên tục sau mỗi giây
final tickerProvider = StreamProvider(
  //strem.periodic tạo ra một dòng phát ra các giá trị theo chu kì thời gian
  (ref) => Stream.periodic(
      const Duration(seconds: 1),
      //là hàm callback được gọi mỗi khi giá trị mới sinh ra. Tăng lên 1 mỗi 1s
      (i) => i + 1),
);

//một streamProvider khác
final namesProvider = StreamProvider((ref) {
  //đọc giá trị từ ticketProvider. Khi giá trị của ticketProvider thay đổi(mỗi giây)
  // => Kích thích lại namesProvider
  return ref
      .watch(tickerProvider.stream)
      //ánh xạ giá trị event
      .map(
        (event) => 
        //trả về danh sách tên từ vị trí 0 đến event trong danh sách
        names.getRange(0, event),
      );
});

class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final names = ref.watch(namesProvider);
    return Scaffold(
      appBar: AppBar(title: const Text('StreamProvider')),
      body: names.when(
          data: (names) {
            return ListView.builder(
              itemCount: names.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(names.elementAt(index)),
                );
              },
            );
          },
          error: (error, stackTrace) =>
              const Text("Reached the end of the list!"),
          loading: () => const Center(
                child: CircularProgressIndicator(),
              )),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData.dark(),
    );
  }
}

//tập hợp giá trị hằng số
enum City { stockholm, paris, tokyo }

//đặt tên lại dữ liệu có sẵn
typedef WeatherEmoji = String;

//Hàm nhận giá trị city, trả về một Future có kiểu dữ liệu là WeatherEmoji
Future<WeatherEmoji> getWeather(City city) {
  //thời gian giữ hiện tại và khi 'Future' hoàn thành là 1s
  return Future.delayed(
    const Duration(seconds: 1),
    () => {
      City.stockholm: '❄️',
      City.paris: '🌧️',
      City.tokyo: '🌬️',
    }[city]!,
    //() => {...}[city]! : đây là một hàm lambda được dùng để trả về giá trị dựa trên city từ Map,
    //city! chắc chắn giá trị không thể là null
  );
}

//UI writes to this provider
//StateProvider để quản lý trạng thái trong Riverpod
//Được khởi tạo với giá trị ban đầu là null
//Mục đích: để lưu trữ thông tin về City
//giá trị curentCityProvider có thể thay đổi theo thời gian khi người dùng thay đổi City
//Cách sử dụng: Khi cần đọc hay cập nhật giá trị của curentCityProvider trong bất cứ ưidget nào cứ dùng ref
//Example: final city = ref.read(currentCityProvider).state;
final currentCityProvider = StateProvider<City?>(
  (ref) => null,
);

const unknowWeatherEmoji = '🤔';

//UI read this
//FutureProvider là thư viện quản lý trạng thái Riverpod
//dùng để quản lý 1 giá trị bất đồng bộ (Future) và cung cấp nó cho các widget con
//Mục đích: giá trị của weatherProvider là một Future chứa WeatherEmoji,
//dùng để lưu trữ thông tin về weather for city
//Cách sử dụng: Khi cần đọc giá trị từ weatherProvider từ bất cứ widget nào, sử dụng ProviderReference(ref)
//giá trị của ưeatherProvider là một Future, vì vậy phải đợi kết quả
//Example:  final weatherFuture = ref.read(weatherProvider)
////weatherFuture.when()
final weatherProvider = FutureProvider<WeatherEmoji>((ref) {
  //city chứa giá trị
  final city = ref.watch(currentCityProvider);
  if (city != null) {
    //get weather city map future
    return getWeather(city);
  } else {
    return unknowWeatherEmoji;
  }
});

class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //theo dõi trạng thái của weatherProvider
    final currentWeather = ref.watch(weatherProvider);

    return Scaffold(
      appBar: AppBar(title: const Text('Weather')),
      body: Column(children: [
        currentWeather.when(
          data: (data) => Text(
            data,
            style: const TextStyle(fontSize: 40),
          ),
          error: (error, stackTrace) => const Text('Error'),
          loading: () => const Padding(
            padding: EdgeInsets.all(8.0),
            child: CircularProgressIndicator(),
          ),
        ),
        Expanded(
            child: ListView.builder(
          //enum return 3
          itemCount: City.values.length,
          itemBuilder: (context, index) {
            final city = City.values[index];
            //Khi state của currentCityProvider được cập nhật giá trị city (thông qua onTab())
            //thì city sẽ = giá trị và iSelected lúc này bằng true => trailing tick check
            final isSelected = city == ref.watch(currentCityProvider);
            return ListTile(
              title: Text(city.toString()),
              trailing: isSelected ? const Icon(Icons.check) : null,
              onTap: () {
                //cập nhật trạng thái của currentCityProvider về giá trị của city
                ref
                    .read(
                      currentCityProvider.notifier,
                    )
                    .state = city;
              },
            );
          },
        ))
      ]),
    );
  }
}

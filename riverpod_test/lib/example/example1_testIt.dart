//test extension
//copy all và gọi chạy hàm testIt();

//extension thêm phương thức mới cho kiểu dữ liệu
//ở đây thêm phương thức T (T là con của num)
//operator định nghĩa toán tử +-*/, ở đây là +
extension OptionalInfixAddition<T extends num> on T? {
  T? operator +(T? other) {
    //nếu this != 0 thì shadow nhận this, còn không thì shadow nhận other
    final shadow = this ?? other;
    if (shadow != null) {
      //trả về number shadow + other (other = 0 nếu orther = null)
      return shadow + (other ?? 0) as T;
    } else {
      return null;
    }
  }
}

//hàm này work dựa trên extension OptionalInfixAddition
//khi int2= null => shadow + (other ?? 0) as T --- other ở đây là int2 = null nên sẽ trả về 0, shaldow = int1 = 1
//result = 1
//giả sử int1 = null, int2 = null => result trả về bằng null
void testIt() {
  final int? int1 = 1;
  final int? int2 = null;
  final result = int1 + int2;
  print(result);
}

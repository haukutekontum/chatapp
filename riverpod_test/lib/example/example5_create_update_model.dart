import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uuid/uuid.dart';
import 'package:meta/meta.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData.dark(),
    );
  }
}

@immutable
class Person {
  final String name;
  final int age;
  final String uuid;

  Person({
    required this.name,
    required this.age,
    String? uuid,
  }) : uuid = uuid ?? const Uuid().v4();

  Person updated([String? name, int? age]) {
    return Person(age: age ?? this.age, name: name ?? this.name, uuid: uuid);
  }

  String get displayName => '$name ($age years old)';

  //covariant cho biết rằng trong quá trình kế thừa và triển khai, bạn đang sử dụng kiểu con
  //=> Không gặp lỗi về kiểu
  @override
  //phương thức operator == kiểm tra xem this và other có bằng nhau không
  //operator == so sánh giá trị các trường
  //ví dụ person1 = Person(name: 'John', age: 25);
  //person2 = Person(name: 'John', age: 25);
  //khi person1 == person2 => sẽ trả về false vì đang so sánh ở địa chỉ bộ nhớ
  //khi operator ==(Person other) => sẽ trả về true vì đang so sánh giá trị của các trường
  bool operator ==(covariant Person other) {
    //trả về true nếu hai uuid của 2 Person this và other bằng nhau
    return uuid == other.uuid;
  }

  // hàm này thêm khi bạn override phương thức operator ==
  // tạo tính nhất quán, bảo đảm tính duy nhất và giảm độ phức tạp khi tìm kiếm và thêm
  @override
  int get hashCode => uuid.hashCode;

  @override
  String toString() => 'Person(name: $name, age: $age, uuid: $uuid)';
}

class DataModel extends ChangeNotifier {
  final List<Person> _people = [];

  int get count => _people.length;

  //UnmodifiableListView tạo một phiên bản không thể thay đổi của danh sách list
  //Bên ngoài không thể thêm sửa xóa phần tử ds _people thông qua biến people,
  //lít _people được bảo toàn
  UnmodifiableListView<Person> get people => UnmodifiableListView(_people);

  void add(Person person) {
    _people.add(person);
    notifyListeners();
  }

  void remove(Person person) {
    _people.remove(person);
    notifyListeners();
  }

  void updated(Person updatedPerson) {
    final index = _people.indexOf(updatedPerson);
    final oldPerson = _people[index];
    if (oldPerson.name != updatedPerson.name ||
        oldPerson.age != updatedPerson.age) {
      _people[index] = oldPerson.updated(updatedPerson.name, updatedPerson.age);
      notifyListeners();
    }
  }
}

final peopleProvider = ChangeNotifierProvider((ref) => DataModel());

class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home Page')),
      body: Consumer(builder: (context, ref, child) {
        final dataModel = ref.watch(peopleProvider);
        return ListView.builder(
          itemCount: dataModel.count,
          itemBuilder: (context, index) {
            final person = dataModel.people[index];
            return ListTile(
              title: GestureDetector(
                onTap: () async {
                  final updatedPerson =
                      await createOrUpdatePersonDialog(context, person);
                  if (updatedPerson != null) {
                    dataModel.updated(updatedPerson);
                  }
                },
                child: Text(person.displayName),
              ),
            );
          },
        );
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final person = await createOrUpdatePersonDialog(context);
          if (person != null) {
            final dataModel = ref.read(peopleProvider);
            dataModel.add(person);
          }
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

//khi dùng TextEditingController bạn có thể đọc giá trị được nhập từ user thông qua text
// và cũng có thể thay đổi giá trị này từ code
final nameController = TextEditingController();
final ageController = TextEditingController();

//Hàm trả về null hoặc person -> Future cho thấy hầm cần thời gian để hoàn thành
//BuildContext context: bắt buộc cho nhiều thao tác Futter, hiển thị cây widget hoặc điều hướng màn hình
// tham số để trong [] có thể bị bỏ qua khi gọi hàm, có thể là một Person hoặc null
Future<Person?> createOrUpdatePersonDialog(BuildContext context,
    [Person? existingPerson]) {
  String? name = existingPerson?.name;
  int? age = existingPerson?.age;

  nameController.text = name ?? '';
  ageController.text = age?.toString() ?? '';

//show dialog trả về Person
//pop(person)
  return showDialog<Person?>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Create a person'),
          content: Column(mainAxisSize: MainAxisSize.min, children: [
            TextField(
              controller: nameController,
              decoration:
                  const InputDecoration(labelText: 'Enter name here...'),
              onChanged: (value) => name = value,
            ),
            TextField(
              controller: ageController,
              decoration: const InputDecoration(labelText: 'Enter age here...'),
              onChanged: (value) => age = int.tryParse(value),
            )
          ]),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Cancel')),
            TextButton(
                onPressed: () {
                  if (name != null && age != null) {
                    if (existingPerson != null) {
                      final newPerson = existingPerson.updated(name, age);
                      Navigator.of(context).pop(newPerson);
                    } else {
                      //no existing person then create new one
                      final newPerson = Person(name: name!, age: age!);
                      Navigator.of(context).pop(newPerson);
                    }
                  } else {
                    //no name, no age or both
                    Navigator.of(context).pop();
                  }
                },
                child: const Text('Save'))
          ],
        );
      });
}

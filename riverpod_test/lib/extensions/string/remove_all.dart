extension RemoveAll on String {
  //fold là phương thức lập qua từng phần tử. Nó nhận giá trị khởi tạo và một hàm callback
  //Example: Previous Value: Hello world, how are you? Hello again!, Element: Hello
  //Previous Value:  world, how are you?  again!, Element: world...
  //Dùng fold để cộng hàm sum trong chuỗi
  //Nối chuỗi trong một danh sách
  //thay thế trong chuỗi
  //Hàm replaceAll ở đây thay thế các element thành ''
  //Example:  String originalString = "Hello world, how are you? Hello again!";
  //String replacedString = originalString.replaceAll("Hello", "Hi");
  // Output: "Hi world, how are you? Hi again!"
  String removeAll(Iterable<String> values) => values.fold(
        this,
        (previousValue, element) => previousValue.replaceAll(element, ''),
      );
}

//convert 0x????? or #??????? to Color
import 'dart:ui' show Color;

import 'package:riverpod_test/extensions/string/remove_all.dart';

//# -> ff
//0x -> 8abh8a (Color code)
//String.padLeft(.., ..)
//Example: String originalString = "123";
//String paddedString = originalString.padLeft(5, '0');
// Output: "00123"
//radix: 16 chuyển đổi số nguyên thành hệ cơ số 10
//Ví dụ: 1F -> 31
//Extension nhận giá trị String trả về giá trị Color
extension AsHtmlColorToColor on String{
  Color htmlColorToColor() => Color(
    int.parse(
      removeAll(['0x', '#']).padLeft(8, 'ff'),
      radix: 16,
    )
  );
}
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/constants/firebase_collection_name.dart';
import 'package:riverpod_test/state/constants/firebase_field_name.dart';
import 'package:riverpod_test/state/posts/typedefs/user_id.dart';
import 'package:riverpod_test/state/user_info/models/user_info_model.dart';

final userInforModelProvider =
    StreamProvider.family.autoDispose<UserInforModel, UserId>(
  (ref, UserId userId) {
    final controller = StreamController<UserInforModel>();

    final sub = FirebaseFirestore.instance
        .collection(FirebaseCollectionName.users)
        .where(
          FirebaseFieldName.userId,
          isEqualTo: userId,
        )
        .limit(1)
        .snapshots()
        .listen((snapshot) {
      final doc = snapshot.docs.first;
      final json = doc.data();
      final userInfoModel = UserInforModel.fromJson(
        json,
        userId: userId,
      );
      controller.add(userInfoModel);
    });

    ref.onDispose(() {
      controller.close();
    });

    return controller.stream;
  },
);

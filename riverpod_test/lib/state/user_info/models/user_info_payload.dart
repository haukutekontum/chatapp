import 'dart:collection' show MapView;

import 'package:flutter/foundation.dart' show immutable;
import 'package:riverpod_test/state/constants/firebase_field_name.dart';

@immutable
class UserInfoPayload extends MapView<String, String> {
  // UserInfoPayload(super.map);
  //Kiểu dữ liệu UserId trong state/posts/typedefs/user_id.dart
  UserInfoPayload(
      {required String userId,
      required String? displayName,
      required String? email})
      : super({
          FirebaseFieldName.userId: userId,
          FirebaseFieldName.displayName: displayName ?? '',
          FirebaseFieldName.email: email ?? '',
        });
}

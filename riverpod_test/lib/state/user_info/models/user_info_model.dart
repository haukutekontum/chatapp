import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:riverpod_test/state/constants/firebase_field_name.dart';
import 'package:riverpod_test/state/posts/typedefs/user_id.dart';

@immutable
class UserInforModel extends MapView<String, String?> {
  final UserId userId;
  final String displayName;
  final String? email;

  UserInforModel({
    required this.email,
    required this.displayName,
    required this.userId,
  }) : super(
          {
            FirebaseFieldName.userId: userId,
            FirebaseFieldName.displayName: displayName,
            FirebaseFieldName.email: email,
          },
        );

  UserInforModel.fromJson(
    Map<String, dynamic> json, {
    required UserId userId,
  }) : this(
          userId: userId,
          displayName: json[FirebaseFieldName.displayName] ?? '',
          email: json[FirebaseFieldName.email],
        );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserInforModel &&
          runtimeType == other.runtimeType &&
          userId == other.userId &&
          displayName == other.displayName &&
          email == other.email;

  @override
  int get hashCode => Object.hashAll([
        userId,
        displayName,
        email,
      ]);
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:riverpod_test/state/auth/constants/constants.dart';
import 'package:riverpod_test/state/auth/models/auth_result.dart';
import 'package:riverpod_test/state/posts/typedefs/user_id.dart';

class Authenticator {

  const Authenticator();

  UserId? get userId => FirebaseAuth.instance.currentUser?.uid;
  bool get isAlreadyLoggedIn => userId != null;
  String get displayName =>
      FirebaseAuth.instance.currentUser?.displayName ?? '';
  String? get email => FirebaseAuth.instance.currentUser?.email;

  //Log out
  Future<void> logOut() async {
    await FirebaseAuth.instance.signOut();
    await GoogleSignIn().signOut();
    await FacebookAuth.instance.logOut();
  }

  Future<AuthResult> loginWithFacebook() async {
    final loginResult = await FacebookAuth.instance.login();
    //accessToken tạo ra khi người dùng đăng nhập thành công
    final token = loginResult.accessToken?.token;
    if (token == null) {
      //user has aborted the loggin in process
      return AuthResult.aborted;
    }

    // Dùng để tạo một FacebookAuthCredential từ FacebookAccessToken
    //sử dụng để đăng nhập vào Firebase Authentication
    final oauthCredentials = FacebookAuthProvider.credential(token);

    try {
      //Đăng nhập vào Firebase qua FacebookAuthCredential
      //Trả về AuthResult.success nếu thành công
      await FirebaseAuth.instance
          .signInWithCredential(oauthCredentials);
      return AuthResult.success;
    }
    //on: để xác định ngoại lệ muốn bắt
    on FirebaseAuthException catch (e) {
      final email = e.email;
      final credential = e.credential;
      if (e.code == Constants.accountExistsWithDifferentCredential &&
          email != null &&
          credential != null) {
        //fetchSignInMethodsForEmail trả về phương thức đăng nhập trước đó,
        //nếu rỗng nghĩa là email chưa đăng kí bất kì tài khoản nào
        final providers =
            await FirebaseAuth.instance.fetchSignInMethodsForEmail(email);
        if (providers.contains(Constants.googleCom)) {
          await loginWithGoogle();
          //linkWithCredential() liên kết tài khoản Google hoặc Facebook
          //với tài khoản người dùng hiện tại đăng nhập bằng email, password
          //=> Đa phương thức đăng nhập
          FirebaseAuth.instance.currentUser?.linkWithCredential(credential);
        }
        return AuthResult.success;
      }
      return AuthResult.failure;
    }
  }

  Future<AuthResult> loginWithGoogle() async {
    final GoogleSignIn googleSignIn =
        GoogleSignIn(scopes: [Constants.emailScope]);

    final signInAccount = await googleSignIn.signIn();
    if (signInAccount == null) {
      return AuthResult.aborted;
    }
    //signInAccount.authentication chứa thông tin xác thực bao gồm accessTojen và idToken
    //accessToken: là là chuỗi token sử dụng thực hiện yêu cầu các dịch vụ của Google
    //idToken: là chuỗi JWT(Json Web Token) chứa thông tin về người dùng và quyển truy cập
    final googleAuth = await signInAccount.authentication;
    final oauthCredentials = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    try {
      await FirebaseAuth.instance.signInWithCredential(oauthCredentials);
      return AuthResult.success;
    } catch (e) {
      return AuthResult.failure;
    }
  }
}

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/auth/models/auth_result.dart';
import 'package:riverpod_test/state/auth/providers/auth_state_provider.dart';

final isLogginedInProvider = Provider<bool>(
  (ref) {
    final authState = ref.watch(authStateProvider);
    return authState.result == AuthResult.success;
  },
);

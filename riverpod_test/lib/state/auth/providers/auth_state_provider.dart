import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/auth/models/auth_state.dart';
import 'package:riverpod_test/state/auth/notifiers/auth_state_notifier.dart';

final authStateProvider = StateNotifierProvider<AuthStateNotifier, AuthState>(
  (_) => AuthStateNotifier(),
);

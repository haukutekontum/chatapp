import 'package:flutter/foundation.dart' show immutable;

@immutable
class Constants {
  static const accountExistsWithDifferentCredential =
      'account-exists-with-different-credential';
  static const googleCom = 'google.com';
  static const emailScope = 'email';
  // dòng này làm hàm trở thành hàm tạo riêng tư. Chỉ được truy cập từ bên trong lớp Constants
  //đóng gói các giá trị hằng số và không cần tạo đối tượng để sử dụng chúng
  const Constants._();
}

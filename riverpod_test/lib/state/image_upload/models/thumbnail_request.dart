import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:riverpod_test/state/image_upload/models/file_type.dart';

@immutable
class ThumbnailRequest {
  final File file;
  final FileType fileType;

  const ThumbnailRequest({
    required this.file,
    required this.fileType,
  });

//Sử dụng operator == để so sánh hai đối tượng bằng nhau về giá trị hay không
//so sánh khi sử dụng cấu trúc dữ liệu set hoặc map
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ThumbnailRequest &&
          runtimeType == other.runtimeType &&
          file == other.file &&
          fileType == other.fileType;

  @override
  int get hashCode => Object.hashAll(
        [
          runtimeType,
          file,
          fileType,
        ],
      );
}

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/image_upload/exceptions/could_not_buid_thumbnail_exception.dart';
import 'package:riverpod_test/state/image_upload/extensions/get_image_aspect_ratio.dart';
import 'package:riverpod_test/state/image_upload/models/file_type.dart';
import 'package:riverpod_test/state/image_upload/models/image_with_aspect_ratio.dart';
import 'package:riverpod_test/state/image_upload/models/thumbnail_request.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

//Hàm này chuyển Kiểu dữ diệu ThumbnailRequest về ImageAspectRatio

//FutureProvider: Lấy dữ liệu từ một Future 
//Provider.family à một phương thức được sử dụng để tạo ra một FutureProvider theo mô hình gia đình (family)
//cho phép bạn tạo ra nhiều providers dựa trên một loại tham số cụ thể

// Trong trường hợp này, ThumbnailRequest là tham số của gia đình (mình cung cấp)
//ImageWithAspectRatio là kiểu dữ liệu mà provider sẽ trả về sau khi hoàn thành
final thumbnailProvider =
    FutureProvider.family.autoDispose<ImageWithAspectRatio, ThumbnailRequest>(
  (ref, ThumbnailRequest thumbnailRequest) async {
    final Image image;

    switch (thumbnailRequest.fileType) {
      case FileType.image:
        image = Image.file(
          thumbnailRequest.file,
          fit: BoxFit.fitHeight,
        );
        break;

      //for video
      case FileType.video:
        final thumb = await VideoThumbnail.thumbnailData(
            video: thumbnailRequest.file.path,
            imageFormat: ImageFormat.JPEG,
            quality: 75);
        if (thumb == null) {
          throw const CouldNotBuildThumbnailException();
        }
        image = Image.memory(
          thumb,
          fit: BoxFit.fitHeight,
        );
        break;
    }
    final aspecRatio = await image.getAspectRatio();
    return ImageWithAspectRatio(
      image: image,
      aspectRatio: aspecRatio,
    );
  },
);

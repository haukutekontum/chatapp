import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:riverpod_test/state/image_upload/extensions/to_files.dart';

@immutable
class ImagePickerHelper {
  //call Image Picker
  static final ImagePicker _imagePicker = ImagePicker();

  // return Future<File?> using toFile()
  static Future<File?> pickImageFromGalllery() =>
      _imagePicker.pickImage(source: ImageSource.gallery).toFile();

  // return Future<File?> using toFile()
  static Future<File?> pickVideoFromGallery() =>
      _imagePicker.pickVideo(source: ImageSource.gallery).toFile();
}

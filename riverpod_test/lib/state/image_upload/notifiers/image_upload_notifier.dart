import 'dart:io';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image/image.dart' as img;
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/constants/firebase_collection_name.dart';
import 'package:riverpod_test/state/image_upload/constants/constants.dart';
import 'package:riverpod_test/state/image_upload/exceptions/could_not_buid_thumbnail_exception.dart';
import 'package:riverpod_test/state/image_upload/extensions/get_collection_name_from_file_type.dart';
import 'package:riverpod_test/state/image_upload/extensions/get_image_data_aspect_ratio.dart';
import 'package:riverpod_test/state/image_upload/models/file_type.dart';
import 'package:riverpod_test/state/image_upload/typedefs/is_loading.dart';
import 'package:riverpod_test/state/posts/models/post_payload.dart';
import 'package:riverpod_test/state/posts/typedefs/user_id.dart';
import 'package:uuid/uuid.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import '../../post_settings/models/post_setting.dart';

class ImageUploadNotifier extends StateNotifier<IsLoading> {
  ImageUploadNotifier() : super(false);

  set isLoading(bool value) => state = value;

  Future<bool> upload({
    required File file,
    required FileType fileType,
    required String message,
    required Map<PostSetting, bool> postSettings,
    required UserId userId,
  }) async {
    isLoading = true;

    //Uint8List thường được sử dụng để làm việc với dữ liệu nhị phân, như hình ảnh, 
    //video, âm thanh, và các loại dữ liệu khác cần được biểu diễn bằng chuỗi các byte
    late Uint8List thumbnailUnit8List;

    switch (fileType) {
      //Lấy thông tin của image
      case FileType.image:
        final fileAsImage = img.decodeImage(file.readAsBytesSync());
        if (fileAsImage == null) {
          isLoading = false;
          throw const CouldNotBuildThumbnailException();
        }
        final thumbnail = img.copyResize(
          fileAsImage,
          width: Constants.imageThumbnailWidth,
        );
        final thumbnailData = img.encodeJpg(thumbnail);
        thumbnailUnit8List = Uint8List.fromList(thumbnailData);
        break;

      case FileType.video:
       //Lấy thông tin của video
        final thumb = await VideoThumbnail.thumbnailData(
          video: file.path,
          imageFormat: ImageFormat.JPEG,
          maxHeight: Constants.videoThumbnailMaxHeiht,
          quality: Constants.videoThumbnailQuality,
        );
        if (thumb == null) {
          isLoading = false;
          throw const CouldNotBuildThumbnailException();
        } else {
          thumbnailUnit8List = thumb;
        }
        break;
    }

    //calculate the aspect ratio
    final thumbnailAspectRatio = await thumbnailUnit8List.getAspectRatio();

    //calculate references
    final fileName = const Uuid().v4();

    //create references to the thumbnail and the image itself
    //save image to Firebase Storage
    //save thumbnail
    final thumbnailRef = FirebaseStorage.instance
        .ref()
        .child(userId)
        .child(FirebaseCollectionName.thumbnails)
        .child(fileName);

    //save video or image
    final originalFileRef = FirebaseStorage.instance
        .ref()
        .child(userId)
        .child(fileType.collectionName)
        .child(fileName);

    try {
      //upload the file cho thumbnail trong Storage
      final thumbnailUploadTask =
          await thumbnailRef.putData(thumbnailUnit8List);
      //lấy name của file trong storage để lưu lại thông tin cho Post
      final thumbnailStorageId = thumbnailUploadTask.ref.name;

      //upload the original file trong storage
      final originalFileUploadTask = await originalFileRef.putFile(file);
      final originalFileStorageId = originalFileUploadTask.ref.name;

      //upload the post itself
      final postPayload = PostPayload(
        userId: userId,
        message: message,
        thumbnailUrl: await thumbnailRef.getDownloadURL(),
        fileUrl: await originalFileRef.getDownloadURL(),
        fileType: fileType,
        fileName: fileName,
        aspectRatio: thumbnailAspectRatio,
        thumbnailStorageId: thumbnailStorageId,
        originalFileStorageId: originalFileStorageId,
        postSettings: postSettings,
      );
      await FirebaseFirestore.instance
          .collection(FirebaseCollectionName.posts)
          .add(postPayload);

      return true;
    } catch (_) {
      return false;
    } finally {
      isLoading = false;
    }
  }
}

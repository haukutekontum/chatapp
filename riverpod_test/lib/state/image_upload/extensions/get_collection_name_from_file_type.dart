import 'package:riverpod_test/state/image_upload/models/file_type.dart';

extension CollectionName on FileType{
  String get collectionName{
    switch(this) {
      case FileType.image:
      return 'images';

      case FileType.video:
      return 'videos';
    }
  }
}
import 'dart:async';

import 'package:flutter/material.dart' as material;

extension GetImageAspectRatio on material.Image {
  Future<double> getAspectRatio() async {
    //Completer để quản lý và hoàn thành Future
    final completer = Completer<double>();
    //Xử lý image
    image.resolve(const material.ImageConfiguration()).addListener(
      material.ImageStreamListener(
        (image, synchronousCall) {
          final aspectRatio = image.image.width / image.image.height;
          //giải phóng image khi sử dụng xong
          image.image.dispose();
          //gán completer
          completer.complete(aspectRatio);
        },
      ),
    );
    //completer.future trả về giá trị được truyền vào khi Future hoàn thành
    //ở đây trả về giá trị aspectRatio
    return completer.future;
  }
}

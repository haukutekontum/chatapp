import 'package:flutter/foundation.dart';

@immutable 
class Constants{
  //for photo
  static const imageThumbnailWidth = 150;

  //for video
  static const videoThumbnailMaxHeiht = 400;
  static const videoThumbnailQuality = 75;

  const Constants._();
}
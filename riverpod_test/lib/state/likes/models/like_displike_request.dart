import 'package:flutter/foundation.dart';
import 'package:riverpod_test/state/posts/typedefs/post_id.dart';
import 'package:riverpod_test/state/posts/typedefs/user_id.dart';

@immutable
class LikeDislikeRequest {
  final PostId postId;
  final UserId likedBy;

  const LikeDislikeRequest({
    required this.postId,
    required this.likedBy,
  });
}

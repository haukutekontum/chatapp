import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/constants/firebase_collection_name.dart';
import 'package:riverpod_test/state/constants/firebase_field_name.dart';
import 'package:riverpod_test/state/likes/models/like.dart';
import 'package:riverpod_test/state/likes/models/like_displike_request.dart';

final likeDislikeProvider =
    FutureProvider.family.autoDispose<bool, LikeDislikeRequest>(
  (ref, LikeDislikeRequest request) async {
    final query = FirebaseFirestore.instance
        .collection(FirebaseCollectionName.likes)
        .where(
          FirebaseFieldName.postId,
          isEqualTo: request.postId,
        )
        .where(
          FirebaseFieldName.userId,
          isEqualTo: request.likedBy,
        )
        .get();

//first see if the user has liked the post already or not
    final hasLikes = await query.then(
      (snapshot) => snapshot.docs.isNotEmpty,
    );

    if (hasLikes) {
      try {
        await query.then((snapshot) async {
          for (final doc in snapshot.docs) {
            await doc.reference.delete();
          }
        });
        return true;
      } catch (_) {
        return false;
      }
    } else {
      //post the likes object
      final like = Like(
        postId: request.postId,
        userId: request.likedBy,
        date: DateTime.now(),
      );

      try {
        await FirebaseFirestore.instance
            .collection(FirebaseCollectionName.likes)
            .add(like);
        return true;
      } catch (_) {
        return false;
      }
    }
  },
);

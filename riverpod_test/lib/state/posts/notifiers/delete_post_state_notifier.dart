import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/constants/firebase_collection_name.dart';
import 'package:riverpod_test/state/constants/firebase_field_name.dart';
import 'package:riverpod_test/state/image_upload/extensions/get_collection_name_from_file_type.dart';
import 'package:riverpod_test/state/image_upload/typedefs/is_loading.dart';
import 'package:riverpod_test/state/posts/models/post.dart';
import 'package:riverpod_test/state/posts/typedefs/post_id.dart';

class DeletePostStateNotifier extends StateNotifier<IsLoading> {
  DeletePostStateNotifier() : super(false);

  set isLoading(bool value) => state = value;

  Future<bool> deletePost({required Post post}) async {
    try {
      isLoading = true;
      //delete the post thumbnail
      await FirebaseStorage.instance
          .ref()
          .child(post.userId)
          .child(FirebaseCollectionName.thumbnails)
          .child(post.thumbnailStorageId)
          .delete();

      // //delete the post original file (video or image)
      await FirebaseStorage.instance
          .ref()
          .child(post.userId)
          .child(post.fileType.collectionName)
          .child(post.originalFileStorageId)
          .delete();

      //delete all comments associated with this post
      await _deleteAllDocument(
        postId: post.postId,
        inCollection: FirebaseCollectionName.comments,
      );

      // //delete all likes associated with this post
      await _deleteAllDocument(
        postId: post.postId,
        inCollection: FirebaseCollectionName.likes,
      );

      //finally delete the post itself
      final postInCollection = await FirebaseFirestore.instance
          .collection(FirebaseCollectionName.posts)
          .where(
            FieldPath.documentId,
            isEqualTo: post.postId,
          )
          .limit(1)
          .get();

          for(final post in postInCollection.docs){
            await post.reference.delete();
          }

      return true;
    } catch (error) {
      debugPrint("Error deleting post: $error");
      return false;
    } finally {
      isLoading = false;
    }
  }

  Future<void> _deleteAllDocument(
      {required PostId postId, required String inCollection}) {
    return FirebaseFirestore.instance.runTransaction(
        timeout: const Duration(seconds: 20),
        maxAttempts: 3, (transaction) async {
      final query = await FirebaseFirestore.instance
          .collection(inCollection)
          .where(
            FirebaseFieldName.postId,
            isEqualTo: postId,
          )
          .get();

      //delete all reference in docs
      for (final doc in query.docs) {
        transaction.delete(doc.reference);
      }
    });
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart' show immutable;
import 'package:riverpod_test/state/image_upload/models/file_type.dart';
import 'package:riverpod_test/state/post_settings/models/post_setting.dart';
import 'package:riverpod_test/state/posts/models/post_key.dart';

@immutable
class Post {
  final String postId;
  final String userId;
  final String messgae;
  final DateTime createAt;
  final String thumbnailUrl;
  final String fileUrl;
  //FileType have video and image
  final FileType fileType;
  final String fileName;
  final double aspectRatio;
  final String thumbnailStorageId;
  final String originalFileStorageId;
  final Map<PostSetting, bool> postSettings;

  Post({required this.postId, required Map<String, dynamic> json})
      : userId = json[PostKey.userId],
        messgae = json[PostKey.message],
        createAt = (json[PostKey.createAt] as Timestamp).toDate(),
        thumbnailUrl = json[PostKey.thumbnailUrl],
        fileUrl = json[PostKey.fileUrl],
        fileType = FileType.values.firstWhere(
          (fileType) => fileType.name == json[PostKey.fileType],
          orElse: () => FileType.image,
        ),
        fileName = json[PostKey.originalFileStorageId],
        aspectRatio = json[PostKey.aspectRatio],
        thumbnailStorageId = json[PostKey.thumbnailStorageId],
        originalFileStorageId = json[PostKey.originalFileStorageId],
        postSettings = {
          //return của một cái Map
          for (final map in json[PostKey.postSettings])
            //get Map in Json have to entries first
            //entries get to form: Key: ${entry.key}, Value: ${entry.value}
            for (final entry in map.entries)
              PostSetting.values.firstWhere(
                (element) => element.storageKey == entry.key,
              ): entry.value as bool,
        };

  //postSettings[PostSetting.allowLikes] để lấy key trong map
  //Map<PostSetting, bool> postSettings; ở đây lấy giá trị key là PostSetting
  bool get allowLikes => postSettings[PostSetting.allowLikes] ?? false;
  bool get allowComments => postSettings[PostSetting.allowComments] ?? false;
}

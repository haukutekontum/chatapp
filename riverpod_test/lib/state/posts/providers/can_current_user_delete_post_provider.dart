import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/auth/providers/user_id_provider.dart';
import 'package:riverpod_test/state/posts/models/post.dart';

final canCurrentUserDeletePostProvider = StreamProvider
.family
.autoDispose<bool, Post>(
  (ref, Post post) async*{
    final userId = ref.watch(userIdProvider);
    yield userId == post.userId;
  },);

  // final canCurrentUserDeletePostProvider = StateProvider
  // .autoDispose
  // .family<bool, Post>((ref, Post post) {
  //   final userId = ref.watch(userIdProvider);
  //   return userId == post.userId;
  // },);
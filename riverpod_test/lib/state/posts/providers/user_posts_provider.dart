import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/main.dart';
import 'package:riverpod_test/state/auth/providers/user_id_provider.dart';
import 'package:riverpod_test/state/constants/firebase_collection_name.dart';
import 'package:riverpod_test/state/constants/firebase_field_name.dart';
import 'package:riverpod_test/state/posts/models/post.dart';
import 'package:riverpod_test/state/posts/models/post_key.dart';

//Tác dụng của code này:

//1.Stream Provider: Sử dụng khi muốn lấy dữ liệu từ API và cập nhật giao diện khi dữ liệu thay đổi
//Khi sử dụng StreamProvider các widget con sẽ lắng nghe và tự cập nhật mỗi khi có sự kiện mới phát ra từ Stream
//Khi ưidget được rebuild trạng thái của StreamProvider vẫn dữ nguyên -> Tránh mất dữ liệu
//StreamProvider.autoDispose: sẽ tự động hủy khi không có ưidget nào lắng nghe đến nó -> giải phóng tài nguyên

final userPostProvider = StreamProvider.autoDispose<Iterable<Post>>(
  (ref) {
    final userId = ref.watch(userIdProvider);
    //StreamController quản lý Stream
    //Thêm: controller.sink.add(..)
    //Lắng nghe sự kiện: controller.stream.listen(..)
    //Đóng: controller.close()
    //Hủy: controller.cancel()
    final controller = StreamController<Iterable<Post>>();

    //thêm
    controller.onListen = () {
      controller.sink.add([]);
    };

    final sub = FirebaseFirestore.instance
        //posts
        .collection(FirebaseCollectionName.posts)
        //orderBy: sắp xếp kết quả theo một trường cụ thể
        //ở đây sắp xếp theo giảm dần (từ lớn đến nhỏ)
        .orderBy(FirebaseFieldName.createdAt, descending: true)
        //where: áp dụng điều kiện lọc
        //ở đây đang kiểm tra xem nếu user_id = this.userUd
        .where(PostKey.userId, isEqualTo: userId)
        //trả về một luồng liên tục stream của các sự kiện
        .snapshots()
        //listen: đăng kí một hàm call back
        .listen((event) {
      //Lấy danh sách các tài liệu từ QuerySnapshot
      final documents = event.docs;
      final posts = documents.where((element) =>
              //Kiểm tra điều kiện
              //metadata đại diện cho các thông tin liên quan đến metadata của tài liệu
              //hasPendingWrites kiểm tra xem tài liệu có đang trong trạng thái chờ xử lý ghi chú
              //xảy ra khi có sự thay đổi được tạo ra nhưng chưa được xác nhận hay đồng bộ với FireStore
              //Tóm lại: dòng này check xem post đang có trên Firestore (không trong trạng thái đang chờ ghi chú hay thay đổi)
              !element.metadata.hasPendingWrites).map(
            (e) => Post(
              postId: e.id,
              json: e.data(),
            ),
          );
      //add controller
      controller.sink.add(posts);
    });

    ref.onDispose(() {
      sub.cancel();
      controller.close();
    });

    return controller.stream;
  },
);
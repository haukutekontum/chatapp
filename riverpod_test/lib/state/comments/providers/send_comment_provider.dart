import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/comments/notifiers/send_comment_notifier.dart';
import 'package:riverpod_test/state/image_upload/typedefs/is_loading.dart';

final  =
    StateNotifierProvider<SendCommentNotifier, IsLoading>(
  (_) => SendCommentNotifier(),
);

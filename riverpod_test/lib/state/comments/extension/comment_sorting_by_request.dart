import 'package:riverpod_test/enums/date_sorting.dart';
import 'package:riverpod_test/state/comments/models/comment.dart';
import 'package:riverpod_test/state/comments/models/post_comments_request.dart';

extension Sorting on Iterable<Comment> {
  Iterable<Comment> applySortingFrom(RequestForPostAndComment request) {
    if (request.sortByCreateAt) {
      final sortedDocuments = toList()
        ..sort(((a, b) {
          switch (request.dateSorting) {
            case DateSorting.newestOnTop:
              return b.createAt.compareTo(a.createAt);

            case DateSorting.oldestOnTop:
              return a.createAt.compareTo(b.createAt);
          }
        }));
      return sortedDocuments;
    } else {
      return this;
    }
  }
}

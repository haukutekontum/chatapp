import 'package:flutter/foundation.dart';
import 'package:riverpod_test/enums/date_sorting.dart';
import 'package:riverpod_test/state/posts/typedefs/post_id.dart';

@immutable
class RequestForPostAndComment {
  final PostId postId;
  final bool sortByCreateAt;
  final DateSorting dateSorting;
  final int? limit;

  const RequestForPostAndComment({
    required this.postId,
    this.sortByCreateAt = true,
    this.dateSorting = DateSorting.newestOnTop,
    this.limit,
  });

  @override
  bool operator ==(covariant RequestForPostAndComment other) =>
      postId == other.postId &&
      sortByCreateAt == other.sortByCreateAt &&
      dateSorting == other.dateSorting &&
      limit == other.limit;

  @override
  int get hashCode => Object.hashAll([
        postId,
        sortByCreateAt,
        dateSorting,
        limit,
      ]);
}

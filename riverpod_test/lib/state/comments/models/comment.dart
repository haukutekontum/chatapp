import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:riverpod_test/state/comments/typedefs/comment_id.dart';
import 'package:riverpod_test/state/constants/firebase_field_name.dart';
import 'package:riverpod_test/state/posts/typedefs/post_id.dart';
import 'package:riverpod_test/state/posts/typedefs/user_id.dart';

@immutable
class Comment {
  final CommentId id;
  final String commnent;
  final DateTime createAt;
  final UserId fromUserId;
  final PostId onPostId;

  Comment(
    Map<String, dynamic> json, {
    required this.id,
  })  : commnent = json[FirebaseFieldName.comment],
        createAt = (json[FirebaseFieldName.createdAt] as Timestamp).toDate(),
        fromUserId = json[FirebaseFieldName.userId],
        onPostId = json[FirebaseFieldName.postId];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Comment &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          commnent == other.commnent &&
          createAt == other.createAt &&
          fromUserId == other.fromUserId &&
          onPostId == other.onPostId;

  @override
  int get hashCode => Object.hashAll([
        id,
        commnent,
        createAt,
        fromUserId,
        onPostId,
      ]);
}

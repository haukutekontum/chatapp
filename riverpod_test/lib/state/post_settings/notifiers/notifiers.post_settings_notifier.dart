import 'dart:collection';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/post_settings/models/post_setting.dart';

//using Map<PostSetting, bool>
//Đang return một cái Map có tất cả PostSetting với key là PostSetting và value = true
//ở đây allowLikes: true, allowComments: true
//UnmodifiableMapView để tạo một phiên bản không thể thay đổi của Map
class PostsettingNotifier extends StateNotifier<Map<PostSetting, bool>> {
  PostsettingNotifier()
      : super(UnmodifiableMapView({
        //Đang tạo dữ liệu gán cho map
          for (final setting in PostSetting.values) setting: true,
        //Dòng này trả về allowLikes: true, allowComments: true
        }));

  //Setting cho từng cái một
  void setting(
    PostSetting setting,
    bool value,
  ) {
    //Giữ giá trị hiện tại của setting nếu có
    //Kiểu dữ liệu là bool?
    //state hiện tại là Map<PostSetting, bool>
    final existingValue = state[setting];
    //Check điều kiện nếu giống nhau thì thôi
    if (existingValue == null || existingValue == value) {
      return;
    }
    //set state = state[setting] = value
    state = Map.unmodifiable(Map.from(state)..[setting] = value);
  }
}

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/post_settings/models/post_setting.dart';
import 'package:riverpod_test/state/post_settings/notifiers/notifiers.post_settings_notifier.dart';

final postSettingProvider =
    StateNotifierProvider<PostsettingNotifier, Map<PostSetting, bool>>(
        (ref) => PostsettingNotifier());

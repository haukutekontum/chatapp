import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/auth/providers/is_logged_in_provider.dart';
import 'package:riverpod_test/state/providers/is_loading_provider.dart';
import 'package:riverpod_test/views/components/loading/loading_screen.dart';
import 'package:riverpod_test/views/login/login_view.dart';
import 'package:riverpod_test/views/main/main_view.dart';

import 'firebase_options.dart';

import 'dart:developer' as devtools show log;

//using ref.watch for rebuild the widget
//using ref.listen NOT rebuild, it has abilty to do something
//such as navigate to new screen, show loading...WHEN SOME VALUE CHANGE
//using ref.read NOT rebuild widget, just read value and no need rebuid

extension Log on Object {
  void log() => devtools.log(toString());
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(brightness: Brightness.dark, primarySwatch: Colors.blue),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.blueGrey,
          indicatorColor: Colors.blueGrey),
      home: Consumer(
        builder: (context, ref, child) {
          //take care of displaying the loading screen
          ref.listen<bool>(isLoadingProvider, (_, isLoading) {
            if (isLoading) {
              LoadingScreen.instances().show(context: context);
            } else {
              LoadingScreen.instances().hide();
            }
          });

          //check if is logged in
          final isLoggedIn = ref.watch(isLogginedInProvider);
          return isLoggedIn ? const MainView() : const LoginView();
        },
      ),
    );
  }
}

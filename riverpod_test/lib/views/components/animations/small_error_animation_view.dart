import 'package:riverpod_test/views/components/animations/lottie_animation_view.dart';
import 'package:riverpod_test/views/components/animations/models/lottie_animation.dart';

class SmallErrorAnimationView extends LotttieAnimationView {
  const SmallErrorAnimationView({super.key})
      : super(
          animation: LottieAnimation.smallError,
        );
}

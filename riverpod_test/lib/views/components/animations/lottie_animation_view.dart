import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:riverpod_test/views/components/animations/models/lottie_animation.dart';

//Muốn lấy một lottie thì: Lottie.asset(path, reverse: false, repeat: true);
class LotttieAnimationView extends StatelessWidget {
  final LottieAnimation animation;
  final bool repeat;
  final bool reverse;

  const LotttieAnimationView(
      {super.key,
      required this.animation,
      this.repeat = true,
      this.reverse = false});

  @override
  Widget build(BuildContext context) => Lottie.asset(
        animation.fullPath,
        repeat: repeat,
        reverse: reverse,
      );
}

extension GetFullPath on LottieAnimation {
  String get fullPath => 'assets/animations/$name.json';
}

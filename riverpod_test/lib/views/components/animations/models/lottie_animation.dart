
//Example of enum
//enum LottieAnimation {
//  dataNotFound,
//  empty,
//  loading,
//  error,
//  smallError
//}
//Tác dụng của enum, khi nào thì nên xài enum
//Cái này dùng để xác định giá trị name của từng giá trị của enum
//nếu muốn lấy name data_not_found thì LottieAnimation.dataNotFound.name
enum LottieAnimation{
  dataNotFound(name: 'data_not_found'),
  empty(name: 'empty'),
  loading(name: 'loading'),
  error(name: 'error'),
  smallError(name: 'small_error');

  final String name;
  const LottieAnimation({required this.name});
}
import 'package:riverpod_test/views/components/animations/lottie_animation_view.dart';
import 'package:riverpod_test/views/components/animations/models/lottie_animation.dart';

class LoadingAnimationView extends LotttieAnimationView {
  const LoadingAnimationView({super.key})
      : super(
          animation: LottieAnimation.loading,
        );
}

class LoadingAnimationView1 extends LotttieAnimationView{
  const LoadingAnimationView1({super.key}) : super(animation: LottieAnimation.loading);
}

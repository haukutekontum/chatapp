import 'dart:async';

import 'package:flutter/material.dart';
import 'package:riverpod_test/views/components/constants/strings.dart';
import 'package:riverpod_test/views/components/loading/loading_screen_controller.dart';

class LoadingScreen {
  //contructor private ._ chỉ có thể gọi trong cùng một file
  LoadingScreen._shareInstance();

  //Đảm bảo rằng chỉ có một LoadingScreen trong toàn bộ ứng dụng
  static final LoadingScreen _share = LoadingScreen._shareInstance();
  //dùng factory để tạo đối tượng
  //Chỉ có thể tạo ra một thể hiện của LoadingScreen
  factory LoadingScreen.instances() => _share;

  LoadingScreenController? _controller;

  void show({
    required BuildContext context,
    String text = Strings.loading,
  }) {
    //có thể là true nếu null trả về false
    if(_controller?.update(text) ?? false){
      return;
    }else{
      _controller = showOverlay(context: context, text: text,);
    }
  }

  void hide() {
    _controller?.close();
    _controller = null;
  }

  LoadingScreenController? showOverlay({
    required BuildContext context,
    required String text,
  }) {
    //Overlays được sử dụng để hiện thị các lớp đè(overlays) trên nd chính ứng dụng
    final state = Overlay.of(context);
    if (state == null) {
      return null;
    }

    //add text loading hoặc(something) vào stream xử lý bất đồng bộ
    final textController = StreamController<String>();
    textController.add(text);

    //renderBox biểu diễn giao diện người dùng
    final renderBox = context.findRenderObject() as RenderBox;
    final size = renderBox.size;

    final overlay = OverlayEntry(builder: (context) {
      return Material(
        color: Colors.black.withAlpha(150),
        child: Center(
            child: Container(
          constraints: BoxConstraints(
            maxHeight: size.height * 0.8,
            maxWidth: size.width * 0.8,
            minWidth: size.width * 0.5
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 10,
                ),
                const CircularProgressIndicator(),
                const SizedBox(
                  height: 10,
                ),
                StreamBuilder<String>(
                    stream: textController.stream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Text(
                          snapshot.requireData,
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium
                              ?.copyWith(color: Colors.black),
                        );
                      } else {
                        return Container();
                      }
                    })
              ],
            )),
          ),
        )),
      );
    });

    state.insert(overlay);

    return LoadingScreenController(close: (){
      textController.close();
      overlay.remove();
      return true;
    }, update: (text) {
      textController.add(text);
      return true;
    },);
  }
}

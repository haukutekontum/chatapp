import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/likes/providers/post_likes_count_provider.dart';
import 'package:riverpod_test/state/posts/typedefs/post_id.dart';
import 'package:riverpod_test/views/components/animations/small_error_animation_view.dart';
import 'package:riverpod_test/views/components/constants/strings.dart';

class LikesCountView extends ConsumerWidget {
  final PostId postId;

  const LikesCountView({required this.postId, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final likesCount = ref.watch(
      postlLikesCountProvider(
        postId,
      ),
    );

    return likesCount.when(
      data: (int likesCount) {
        final personOrPeople =
            likesCount == 1 ? Strings.person : Strings.person;
        final likesText = '$likesCount $personOrPeople ${Strings.likesThis}';
        return Text(
          likesText,
        );
      },
      error: (error, stackTrace) {
        return const SmallErrorAnimationView();
      },
      loading: () {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}

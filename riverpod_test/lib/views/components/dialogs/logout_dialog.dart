import 'package:flutter/foundation.dart';
import 'package:riverpod_test/views/components/constants/strings.dart';
import 'package:riverpod_test/views/components/dialogs/alert_dialog_model.dart';

@immutable
class LogoutDialog extends AlertDialogModel<bool>{
  const LogoutDialog() : super(
    title: Strings.logOut,
    message: Strings.areYouSureYouWantToLogOutOfTheApp,
    buttons: const {
      Strings.cancel : false,
      Strings.logOut : true,
      Strings.comment : true,
    }
  );
}

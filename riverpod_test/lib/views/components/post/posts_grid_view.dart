import 'package:flutter/material.dart';
import 'package:riverpod_test/state/posts/models/post.dart';
import 'package:riverpod_test/views/components/post/post_thumbnail_view.dart';
import 'package:riverpod_test/views/post_details/post_details_view.dart';

class PostGridView extends StatelessWidget {
  final Iterable<Post> posts;

  const PostGridView({
    Key? key,
    required this.posts,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        padding: const EdgeInsets.all(8.0),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
        ),
        itemCount: posts.length,
        itemBuilder: (context, index) {
          final post = posts.elementAt(index);
          return PostThumbnailView(
            post: post,
            onTapped: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => PostDetailsView(
                    post: post,
                  ),
                ),
              );
            },
          );
        });
  }
}

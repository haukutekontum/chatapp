import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:riverpod_test/state/image_upload/models/file_type.dart';
import 'package:riverpod_test/state/posts/models/post.dart';
import 'package:riverpod_test/views/components/post/post_image_view.dart';
import 'package:riverpod_test/views/components/post/post_video_view.dart';

class PostImageOrVideoVIew extends StatelessWidget {
  final Post post;

  const PostImageOrVideoVIew({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    switch (post.fileType) {
      case FileType.image:
        return PostImageView(
          post: post,
        );

      case FileType.video:
        return PostVideoView(
          post: post,
        );

      default:
        return const SizedBox();
    }
  }
}

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:riverpod_test/views/components/rich_text/base_text.dart';
import 'package:riverpod_test/views/components/rich_text/link_text.dart';

class RichTextWidget extends StatelessWidget {
  final Iterable<BaseText> texts;
  final TextStyle? styleForAll;

  const RichTextWidget({
    super.key,
    required this.texts,
    this.styleForAll,
  });

  @override
  Widget build(BuildContext context) {
    return RichText(
        text: TextSpan(
      children: texts.map((baseText) {
        if (baseText is LinkText) {
          return TextSpan(
              text: baseText.text,
              //merge 2 style Text lại với nhau kế thừa cả hai style
              style: styleForAll?.merge(baseText.style),
              //TapGestureRecognizer lắng nghe sự kiện nhấn vào một phần của RichText
              recognizer: TapGestureRecognizer()..onTap = baseText.onTapped);
        } else {
          return TextSpan(
              text: baseText.text,
              style: styleForAll?.merge(baseText.style),);
        }
      }).toList(),
    ));
  }
}

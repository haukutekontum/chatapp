import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/auth/providers/auth_state_provider.dart';
import 'package:riverpod_test/state/image_upload/helpers.image_picker_helper.dart';
import 'package:riverpod_test/state/image_upload/models/file_type.dart';
import 'package:riverpod_test/state/post_settings/providers/post_settings_provider.dart';
import 'package:riverpod_test/views/components/dialogs/alert_dialog_model.dart';
import 'package:riverpod_test/views/components/dialogs/logout_dialog.dart';
import 'package:riverpod_test/views/constants/strings.dart';
import 'package:riverpod_test/views/create_new_post/create_new_post_view.dart';
import 'package:riverpod_test/views/tabs/home/home_view.dart';
import 'package:riverpod_test/views/tabs/search/search_view.dart';
import 'package:riverpod_test/views/tabs/users_posts/user_posts_view.dart';

class MainView extends ConsumerStatefulWidget {
  const MainView({Key? key}) : super(key: key);

  @override
  ConsumerState<MainView> createState() => _MyWidgetState();
}

class _MyWidgetState extends ConsumerState<MainView> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: const Text(Strings.appName),
          actions: [
            //Add Video
            IconButton(
              icon: const FaIcon(
                FontAwesomeIcons.film,
              ),
              onPressed: () async {
                //pick video
                final videoFile =
                    await ImagePickerHelper.pickVideoFromGallery();
                if (videoFile == null) {
                  return;
                }
                ref.refresh(postSettingProvider);

                //go to the screen to create a new post
                if (!mounted) {
                  return;
                }

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => CreateNewPostView(
                      fileToPost: videoFile,
                      fileType: FileType.video,
                    ),
                  ),
                );
              },
            ),
            //Add Post image
            IconButton(
              icon: const Icon(
                Icons.add_photo_alternate_outlined,
              ),
              onPressed: () async {
                //pick image fro gallary
                final imageFile =
                    await ImagePickerHelper.pickImageFromGalllery();
                if (imageFile == null) {
                  return;
                }
                //need to refresh the choose postSetting 
                //it have to be all allow likes and comments to true
                ref.refresh(postSettingProvider);

                //mounted: kiểm tra cây widget. trả về true nếu state của widget được thêm vào
                //trả về false nếu widget bị loại bỏ khỏi cây widget
                //dùng mouted sẽ tránh trường hợp khi the widget bị loại bỏ khỏi cây widget vì xử lý đã chuyển
                //sang màn hình khác. mouted đảm bảo rằng người dùng không thay đổi vào UI(bấm button khác..) 
                //của một wiget đã bị loại bỏ
                if (!mounted) {
                  return;
                }

                //go to the screen to create a new post
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => CreateNewPostView(
                      fileToPost: imageFile,
                      fileType: FileType.image,
                    ),
                  ),
                );
              },
            ),
            //Logout
            IconButton(
              onPressed: () async {
                final shouldLogOut =
                    await const LogoutDialog().present(context).then(
                          (value) => value ?? false,
                        );
                if (shouldLogOut) {
                  await ref.read(authStateProvider.notifier).logOut();
                }
              },
              icon: const Icon(
                Icons.logout,
              ),
            ),
          ],
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.person),
              ),
              Tab(
                icon: Icon(Icons.search),
              ),
              Tab(
                icon: Icon(Icons.home),
              ),
            ],
          ),
        ),
        //View
        body: const TabBarView(children: [
          UserPostsView(),
          searchView(),
          HomeView(),
        ]),
      ),
    );
  }
}

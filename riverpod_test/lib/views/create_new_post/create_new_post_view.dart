import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/state/auth/providers/user_id_provider.dart';
import 'package:riverpod_test/state/image_upload/models/file_type.dart';
import 'package:riverpod_test/state/image_upload/models/thumbnail_request.dart';
import 'package:riverpod_test/state/image_upload/providers/image_uploader_provider.dart';
import 'package:riverpod_test/state/post_settings/models/post_setting.dart';
import 'package:riverpod_test/state/post_settings/providers/post_settings_provider.dart';
import 'package:riverpod_test/views/components/file_thumbnail_view.dart';
import 'package:riverpod_test/views/constants/strings.dart';

//Sử dụng HookConsumerWidget
class CreateNewPostView extends StatefulHookConsumerWidget {
  //Nhận file từ ImagePicker
  final File fileToPost;
  final FileType fileType;

  const CreateNewPostView({
    required this.fileToPost,
    required this.fileType,
    super.key,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _CreateNewPostViewState();
}

class _CreateNewPostViewState extends ConsumerState<CreateNewPostView> {
  @override
  Widget build(BuildContext context) {
    //gán dữ liệu cho thumbnailRequest
    final thumbnailRequest =
        ThumbnailRequest(file: widget.fileToPost, fileType: widget.fileType);
    final postSettings = ref.watch(postSettingProvider);
    //theo dõi giá trị TextField
    final postController = useTextEditingController();

    //Hook is here:
    //useState: quản lý trạng thái widget functional
    //ở đây state kiểu bool, có giá trị khởi tạo là false
    final isPostButtonEnabled = useState(false);

    //useEffect: thực hiện các hiệu ứng trong widget khi có sự thay đổi trong các phụ thuộc được chỉ định
    //useEffect((){}), [postController]): thực hiện các hiệu ứng khi có sự thay đổi phụ thuộc
    //ở đây là postController
    //Hàm callback bên trong useEffect sử được thực hiện khi postController thay đổi
    useEffect(() {
      //thực hiện các công việc khi có sư thay đổi phụ thuộc
      void listener() {
        //postController.text không rỗng trả về true, còn rỗng trả về false
        isPostButtonEnabled.value = postController.text.isNotEmpty;
      }

      //addListener để đăng kí lắng nghe sự thay đổi trong hàm listener
      postController.addListener(listener);

      //hàm clean up, sẽ được gọi khi widget bị hủy bỏ hoặc có sự thay đổi phụ thuộc
      //removeLisner để gỡ bỏ listener tránh bị rò rỉ bộ nhớ
      return () {
        postController.removeListener(listener);
      };
    }, [postController]);

    return Scaffold(
      appBar: AppBar(
        title: const Text(Strings.createNewPost),
        actions: [
          IconButton(
            icon: const Icon(Icons.send),
            //Check if text field is not empty
            onPressed: isPostButtonEnabled.value
                ? () async {
                    final userId = ref.read(
                      userIdProvider,
                    );
                    if (userId == null) {
                      return;
                    }
                    final message = postController.text;
                    //Save post to Firebase
                    final isUpload =
                        await ref.read(imageUploadProvider.notifier).upload(
                              file: widget.fileToPost,
                              fileType: widget.fileType,
                              message: message,
                              postSettings: postSettings,
                              userId: userId,
                            );
                    if (isUpload && mounted) {
                      Navigator.of(context).pop();
                    }
                  }
                : null,
          ),
        ],
      ),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FileThumbnailView(
            thumbnailRequest: thumbnailRequest,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              decoration: const InputDecoration(
                  labelText: Strings.pleaseWriteYourMessageHere),
              autofocus: true,
              maxLines: null,
              controller: postController,
            ),
          ),

          //show enum PostSetting listTitle
          ...PostSetting.values.map((postSetting) => ListTile(
                title: Text(
                  postSetting.title,
                ),
                subtitle: Text(postSetting.description),
                trailing: Switch(
                  value: postSettings[postSetting] ?? false,
                  onChanged: (isOn) {
                    ref.read(postSettingProvider.notifier).setting(
                          postSetting,
                          isOn,
                        );
                  },
                ),
              ))
        ],
      )),
    );
  }
}

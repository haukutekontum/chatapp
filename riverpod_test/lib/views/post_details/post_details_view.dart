import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_test/enums/date_sorting.dart';
import 'package:riverpod_test/state/comments/models/post_comments_request.dart';
import 'package:riverpod_test/state/comments/models/post_with_comment.dart';
import 'package:riverpod_test/state/posts/models/post.dart';
import 'package:riverpod_test/state/posts/providers/can_current_user_delete_post_provider.dart';
import 'package:riverpod_test/state/posts/providers/delete_post_provider.dart';
import 'package:riverpod_test/state/posts/providers/specific_with_comments_provider.dart';
import 'package:riverpod_test/views/components/animations/small_error_animation_view.dart';
import 'package:riverpod_test/views/components/comment/compact_comment_column.dart';
import 'package:riverpod_test/views/components/dialogs/alert_dialog_model.dart';
import 'package:riverpod_test/views/components/dialogs/delete_dialog.dart';
import 'package:riverpod_test/views/components/like_button.dart';
import 'package:riverpod_test/views/components/likes_count_view.dart';
import 'package:riverpod_test/views/components/post/post_date_view.dart';
import 'package:riverpod_test/views/components/post/post_display_name_and_message.dart';
import 'package:riverpod_test/views/components/post/post_image_or_video_view.dart';
import 'package:riverpod_test/views/constants/strings.dart';
import 'package:riverpod_test/views/post_comment/post_comments_view.dart';
import 'package:share_plus/share_plus.dart';

class PostDetailsView extends ConsumerStatefulWidget {
  final Post post;
  const PostDetailsView({required this.post, super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _PostDetailsViewState();
}

class _PostDetailsViewState extends ConsumerState<PostDetailsView> {
  @override
  Widget build(BuildContext context) {
    final request = RequestForPostAndComment(
      postId: widget.post.postId,
      limit: 3,
      sortByCreateAt: true,
      dateSorting: DateSorting.oldestOnTop,
    );

    //Get list comment with post base on request
    final postWithComment = ref.watch(
      specificPostWithCommentProvider(request),
    );

    //can we delete this post
    final canDeletePost = ref.watch(canCurrentUserDeletePostProvider(
      widget.post,
    ));

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          Strings.postDetails,
        ),
        actions: [
          //share button is always present
          postWithComment.when(data: (postWithComment) {
            return IconButton(
              onPressed: () {
                final url = postWithComment.post.fileUrl;
                Share.share(
                  url,
                  subject: Strings.checkOutThisPost,
                );
              },
              icon: const Icon(Icons.share),
            );
          }, error: (error, stackTrace) {
            return const SmallErrorAnimationView();
          }, loading: () {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }),

          //delete button or no delete button
          if (canDeletePost.value ?? false)
            IconButton(
              onPressed: () async {
                final shouldDeletePost = await const DeleteDialog(
                  titleOfObjectToDelete: Strings.post,
                )
                    .present(context)
                    .then((shouldDelete) => shouldDelete ?? false);

                if (shouldDeletePost) {
                  await ref.read(deletePostProvider.notifier).deletePost(
                        post: widget.post,
                      );
                  if (mounted) {
                    Navigator.of(context).pop();
                  }
                }
              },
              icon: const Icon(Icons.delete),
            ),
        ],
      ),
      body: postWithComment.when(
        data: (postWithComment) {
          final postId = postWithComment.post.postId;
          return SingleChildScrollView(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  PostImageOrVideoVIew(
                    post: postWithComment.post,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      //Like button if post allows liking
                      if (postWithComment.post.allowLikes)
                        LikeButton(postId: postId),

                      //Comment button if allows comment
                      if (postWithComment.post.allowComments)
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    PostCommentsView(postId: postId),
                              ),
                            );
                          },
                          icon: const Icon(Icons.mode_comment_outlined),
                        ),
                    ],
                  ),

                  //post details (show divider at bottom)
                  PostDisplayNameAndMessageView(
                    post: postWithComment.post,
                  ),
                  PostDateView(
                    dateTime: postWithComment.post.createAt,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Divider(
                      color: Colors.white70,
                    ),
                  ),
                  //comments
                  CompactCommentColumn(
                    comments: postWithComment.comments,
                  ),
                  //diplay like count
                  if (postWithComment.post.allowLikes)
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          LikesCountView(postId: postId),
                        ],
                      ),
                    ),

                  //and spacing to bottom of screen
                  const SizedBox(
                    height: 100,
                  ),
                ]),
          );
        },
        error: (error, stackTrace) {
          return const SmallErrorAnimationView();
        },
        loading: () {
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
